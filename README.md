Database Used - MySQL

Scripts for creating schema, user, tables and sample data are located inside the project. (src\main\resources\DB_Scripts\Create_User_Database_Tables_Data.sql)

I used Lombok for this project. Please download the .jar here (https://projectlombok.org/download)
Once downloaded, run the .jar  and an installer UI will open. Select the IDE you are using and click Install/Update

List of API

Login API (POST) - http://localhost:8080/api/user/login
{
    "username" : "user",
    "password" : "user"
}

List of Basket Items (GET) - http://localhost:8080/api/basket/getBasket?userId=

Search Product by Name or by Type (GET) - http://localhost:8080/api/product?productName=&productType=

Save or Add Product to Basket(POST) - http://localhost:8080/api/basket/save
{
    "productId" : 3,
    "userId" : 1,
    "quantity" : 2

}

Check if Product already bought by the user (POST) - http://localhost:8080/api/basket/productExist
{
    "productId" : 4,
    "userId" : 1

}

