package com.rs2.exercise;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rs2.exercise.controller.ProductController;
import com.rs2.exercise.response.ProductResponse;
import com.rs2.exercise.service.ProductService;

@WebMvcTest(ProductController.class)
public class ProductControllerTest {
	@Autowired
	MockMvc mockMvc;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@MockBean
	ProductService productService;
	
	ProductResponse product1 = new ProductResponse(1L, "Abook", "Books", "A Book");
	ProductResponse product2 = new ProductResponse(2L, "Amusic", "Music", "A Music");
	ProductResponse product3 = new ProductResponse(3L, "Agames", "Games", "A Games");
	
	@Test
	public void getProduct_success() throws Exception {
		List<ProductResponse> listProduct = new ArrayList<>();
		listProduct.add(product1);
		listProduct.add(product2);
		listProduct.add(product3);
		
		Mockito.when(productService.getProduct("", "Books")).thenReturn(listProduct);
		
		mockMvc.perform(MockMvcRequestBuilders
				.get("/api/product")
				.contentType(MediaType.APPLICATION_JSON)).andReturn();
	}
}
