package com.rs2.exercise.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs2.exercise.entity.Product;
import com.rs2.exercise.repository.ProductRepository;
import com.rs2.exercise.response.ProductResponse;

@Service
public class ProductService {
	@Autowired
	private ProductRepository productRepository;
	
	public List<ProductResponse> getProduct(String productName, String productType) {
		List<Product> productList;
		List<ProductResponse> listProductResponses = new ArrayList<>();
		
		if (productType == null) {
			productList = productRepository.getProductByName(productName);
		} else {
			productList = productRepository.getProduct(productName, productType);
		}
		
		productList.stream().forEach(e -> {
			ProductResponse productResponse = new ProductResponse(e.getProductId(), e.getProductName(), e.getProductType(), e.getProductDescription());
			listProductResponses.add(productResponse);
		});
		
		return listProductResponses;
	}

}
