package com.rs2.exercise.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.rs2.exercise.entity.Basket;
import com.rs2.exercise.repository.BasketRepository;
import com.rs2.exercise.response.BasketResponse;
import com.rs2.exercise.response.BasketResponses;

@Service
public class BasketService {
	@Autowired
	BasketRepository basketRepository;
	
	public ResponseEntity<?> saveProduct(Basket basket) {
		Basket saveResult = basketRepository.save(basket);
		
		if (saveResult != null) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	public List<BasketResponse> getBasketItems(long userId) {
		List<BasketResponses> basketList = basketRepository.getBasketItems(userId);
		List<BasketResponse> listBasketResponses = new ArrayList<>();
		
		basketList.stream().forEach(e -> {
			BasketResponse basketResponse = new BasketResponse(e.getQuantity(), e.getProductName(), e.getProductType(), e.getProductDescription());
			listBasketResponses.add(basketResponse);
		});
		
		return listBasketResponses;
	}
	
	public ResponseEntity<?> productNotExist(Basket basket) {
		List<Basket> productExist = basketRepository.findByProductIdAndUserId(basket.getProductId(), basket.getUserId());
		
		if (productExist.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
