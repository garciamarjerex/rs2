package com.rs2.exercise.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.rs2.exercise.criteria.UserLoginCriteria;
import com.rs2.exercise.entity.User;
import com.rs2.exercise.repository.UserRepository;
import com.rs2.exercise.response.UserLoginResponse;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	
	public UserLoginResponse userLogin(UserLoginCriteria userLoginCriteria) {
		List<User> listUser = (List<User>) userRepository.findAll();
		UserLoginResponse userLoginResponse = new UserLoginResponse();
		
		listUser.stream().forEach(e -> {
			if (userLoginCriteria.getUsername().equalsIgnoreCase(e.getUserName()) && userLoginCriteria.getPassword().equalsIgnoreCase(e.getPassword())) {
				userLoginResponse.setLoginStatus("Success");
				userLoginResponse.setUserId(e.getUserId());
			}
		});
		
		return userLoginResponse;
	}
}
