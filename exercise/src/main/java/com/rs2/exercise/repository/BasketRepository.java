package com.rs2.exercise.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.rs2.exercise.entity.Basket;
import com.rs2.exercise.response.BasketResponses;

@Repository
public interface BasketRepository extends CrudRepository<Basket, Long>{
	@Query(value = "SELECT b.Quantity AS quantity, p.Name AS productName, p.Type AS productType, p.Description AS productDescription FROM basket b, product p " +
					"WHERE b.ProductID = p.ID AND b.UserID = :userId", nativeQuery = true)
	List<BasketResponses> getBasketItems(@Param("userId") long userId);
	
	List<Basket> findByProductIdAndUserId(long productId, long userId);
}