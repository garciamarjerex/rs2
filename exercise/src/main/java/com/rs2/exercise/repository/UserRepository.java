package com.rs2.exercise.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.rs2.exercise.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{

}
