package com.rs2.exercise.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BasketResponse {
	private long quantity;
	private String productName;
	private String productType;
	private String productDescription;
}
