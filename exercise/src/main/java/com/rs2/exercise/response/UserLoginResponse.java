package com.rs2.exercise.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserLoginResponse {
	private String loginStatus;
	private long userId;
}
