package com.rs2.exercise.response;

public interface BasketResponses {
	long getQuantity();
	String getProductName();
	String getProductType();
	String getProductDescription();
}
