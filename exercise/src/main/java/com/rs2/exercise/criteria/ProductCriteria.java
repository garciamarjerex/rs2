package com.rs2.exercise.criteria;

import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductCriteria {
	@Pattern(regexp="^[A-Za-z]*$", message = "Invalid Input")
	private String productName;
	private String productType;
}
