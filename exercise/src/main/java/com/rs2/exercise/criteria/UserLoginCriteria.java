package com.rs2.exercise.criteria;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserLoginCriteria {
	@NotBlank
	private String username;
	@NotBlank
	private String password;

}
