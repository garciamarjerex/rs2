package com.rs2.exercise.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "basket")
@Getter
@Setter
public class Basket {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private long basketId;
	@Column(name = "ProductID")
	private long productId;
	@Column(name = "UserID")
	private long userId;
	@Column(name = "Quantity")
	private long quantity;
	
}
