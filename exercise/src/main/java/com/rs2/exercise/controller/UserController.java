package com.rs2.exercise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rs2.exercise.criteria.UserLoginCriteria;
import com.rs2.exercise.response.UserLoginResponse;
import com.rs2.exercise.service.UserService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/user")
public class UserController {
	@Autowired
	UserService userService;

	@PostMapping("/login")
	public ResponseEntity<UserLoginResponse> userLogin(@Valid @RequestBody UserLoginCriteria userLoginCriteria) {
		return ResponseEntity.ok().body(userService.userLogin(userLoginCriteria));
	}
}
