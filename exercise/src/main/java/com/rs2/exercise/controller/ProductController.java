package com.rs2.exercise.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rs2.exercise.criteria.ProductCriteria;
import com.rs2.exercise.response.ProductResponse;
import com.rs2.exercise.service.ProductService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/product")
public class ProductController {
	@Autowired
	ProductService productService;
	
	@GetMapping
	public ResponseEntity<List<ProductResponse>> getProduct(@Valid @RequestBody ProductCriteria productCriteria) {
		return ResponseEntity.ok().body(productService.getProduct(productCriteria.getProductName(), productCriteria.getProductType()));
	}
}
