package com.rs2.exercise.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rs2.exercise.entity.Basket;
import com.rs2.exercise.response.BasketResponse;
import com.rs2.exercise.service.BasketService;

@RestController
@RequestMapping("/api/basket")
public class BasketController {
	@Autowired
	BasketService basketService;
	
	@PostMapping("/save")
	public ResponseEntity<?> saveProduct(@RequestBody Basket basket) {
		return ResponseEntity.ok().body(basketService.saveProduct(basket));
	}
	
	@GetMapping("/getBasket")
	public ResponseEntity<List<BasketResponse>> getBasketItems(@RequestParam("userId") long userId) {
		return ResponseEntity.ok().body(basketService.getBasketItems(userId));
	}
	
	@PostMapping("/productExist")
	public ResponseEntity<?> productNotExist(@RequestBody Basket basket) {
		return ResponseEntity.ok().body(basketService.productNotExist(basket));
	}
}
