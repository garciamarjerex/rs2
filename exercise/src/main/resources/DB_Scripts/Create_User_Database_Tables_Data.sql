CREATE DATABASE rs2;
USE rs2;

CREATE USER rs2user IDENTIFIED BY 'rs2';
GRANT ALL ON rs2.* TO rs2user;

CREATE TABLE `rs2`.`product` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(32) NOT NULL,
  `Type` VARCHAR(8) NOT NULL,
  `Description` VARCHAR(100) NULL,
  PRIMARY KEY (`ID`));
  
  
CREATE TABLE `rs2`.`user` (
  `UserID` INT NOT NULL AUTO_INCREMENT,
  `LoginName` VARCHAR(20) NOT NULL,
  `Password` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`UserID`));


CREATE TABLE `rs2`.`basket` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `ProductID` INT NOT NULL,
  `UserID` INT NOT NULL,
  `Quantity` INT NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `FOREIGN_KEY_PRODUCT_ID_idx` (`ProductID` ASC) VISIBLE,
  INDEX `FOREIGN_KEY_USER_ID_idx` (`UserID` ASC) VISIBLE,
  CONSTRAINT `FOREIGN_KEY_PRODUCT_ID`
    FOREIGN KEY (`ProductID`)
    REFERENCES `rs2`.`product` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FOREIGN_KEY_USER_ID`
    FOREIGN KEY (`UserID`)
    REFERENCES `rs2`.`user` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


INSERT INTO `rs2`.`product`
(`Name`, `Type`, `Description`)
VALUES
('RS2Books', 'Books', 'RS2 Books'),
('RS2Music', 'Music', 'RS2 Music'),
('RS2Games', 'Games', 'RS2 Games');

INSERT INTO `rs2`.`user`
(`LoginName`, `Password`)
VALUES
('user', 'user');